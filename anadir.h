#ifndef ANADIR_H
#define ANADIR_H

#include <QDialog>
#include <producto.h>
#include <data.h>
#include <QRegExp>
#include <QValidator>
namespace Ui {
class Anadir;
}

class Anadir : public QDialog
{
    Q_OBJECT

public:
    explicit Anadir(QWidget *parent = 0);
    ~Anadir();

private slots:
    void on_baceptar_clicked();

private:
    Ui::Anadir *ui;
};

#endif // ANADIR_H
