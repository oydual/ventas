#include "producto.h"
#include <QDebug>

int Producto::count_id = 0;

Producto::Producto()
{
    this->_cant = 0;
    this->_id= 0;
    this->_precio = 0.0f;
    this->_tipo = otro;
    this->_nombre = "";
}
Producto::Producto(int cant, int id , float precio, Tipo tipo,QString nombre) : _id(id) , _tipo(tipo) , _cant(cant), _precio(precio), _nombre(nombre)
{

}
Producto::Producto(const Producto &producto)
{
    this->_cant = producto._cant;
    this->_id= producto._id;
    this->_precio = producto._precio;
    this->_tipo = producto._tipo;
    this->_nombre = producto._nombre;
}

void Producto::setId(int id)
{
    this->_id = id;
}
void Producto::setCant(int cant)
{
    this->_cant = cant;
}
void Producto::setPrecio(float precio)
{
    this->_precio = precio;
}
void Producto::setTipo(Tipo tipo)
{
    this->_tipo = tipo;
}
void Producto::setNombre(QString nombre)
{
    this->_nombre = nombre;
}

int Producto::getId()
{
    return this->_id;
}
int Producto::getCant()
{
    return this->_cant;
}
float Producto::getPrecio()
{
    return this->_precio;
}
Tipo Producto::getTipo()
{
    return this->_tipo;
}
QString Producto::getNombre()
{
    return this->_nombre;
}

void Producto::init(int id)
{
    count_id = id;
}
int Producto::act_id()
{
    return count_id;
}
void Producto::add_id()
{
    count_id++;
}

QString Producto::getTipoS()
{
    switch (this->_tipo) {
    case comestible:
        return "Comestible";
        break;
    case higiene:
        return "Higiene";
        break;
    default:
        return "Otro";
        break;
    }
}

ostream& operator<< (ostream &out, const Producto &producto)
{
    out << setw(10) <<producto._id << ":";
    out << setw(10) << producto._cant << ":";
    out << setw(5) << producto._tipo << ":";
    out << setw(10) << producto._precio << ":";
    out << setw(40) << producto._nombre.toStdString() << endl;
    return out;
}
istream& operator>> (istream &in, Producto &producto)
{
    string aux;
    getline(in,aux,':');
    if(in.eof())
    {
        return in;
    }
    producto._id = stoi(aux);
    getline(in,aux,':');
    producto._cant = stoi(aux);
    getline(in,aux,':');
    producto._tipo = (Tipo)stoi(aux);
    getline(in,aux,':');
    producto._precio = stof(aux);
    in >> aux;
    producto._nombre = QString(aux.c_str());
    return in;
}
