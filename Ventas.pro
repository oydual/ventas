#-------------------------------------------------
#
# Project created by QtCreator 2016-11-06T23:22:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Ventas
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    producto.cpp \
    anadir.cpp \
    data.cpp \
    listar.cpp

HEADERS  += mainwindow.h \
    producto.h \
    anadir.h \
    data.h \
    listar.h

FORMS    += mainwindow.ui \
    anadir.ui \
    listar.ui

CONFIG += c++14
