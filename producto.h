#ifndef PRODUCTO_H
#define PRODUCTO_H
#include <fstream>
#include <iostream>
#include <iomanip>
#include <QString>

using namespace std;
enum Tipo{ comestible,
           higiene,
           otro};
class Producto
{
private:
    int _id;
    Tipo _tipo;
    int _cant;
    float _precio;
    QString _nombre;
    static int count_id;
public:
    Producto();
    Producto(int,int,float,Tipo,QString);
    Producto(const Producto&);
    void setId(int);
    void setTipo(Tipo);
    void setCant(int);
    void setPrecio(float);
    void setNombre(QString);
    int getId();
    Tipo getTipo();
    QString getTipoS();
    int getCant();
    float getPrecio();
    QString getNombre();
    static void init(int);
    static int act_id();
    static void add_id();
    friend ostream& operator<< (ostream&, const Producto&);
    friend istream& operator>> (istream&, Producto&);
};

#endif // PRODUCTO_H
