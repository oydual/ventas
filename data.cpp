#include "data.h"

Data* Data::datos = 0;
Data::Data()
{

}
QList<Producto>& Data::getListProductos()
{
    return this->lista_productos;
}
Data* Data::get(){
    if(datos == NULL)
    {
        datos = new Data;
        datos->initProductos();
        atexit(&borrar);
    }
    return datos;
}
void Data::borrar(){
    if(datos != NULL)
    {
        delete datos;
    }
}
void Data::initProductos()
{
    fstream file;
    Producto aux;
    file.open("productos.dat",ios_base::in);
    while(file >> aux)
    {

        if(!file.eof())
        {
            lista_productos.push_back(aux);
        }
    }
    file.close();
}
bool Data::write(QString s, void *obj)
{
    fstream file;
    if(s ==  "Producto")
    {
        file.open("productos.dat",ios_base::out | ios_base::app);
        file << *((Producto*)obj);
        return true;
    }
    return false;
}
