#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    QFile file("config.dat");
    file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
    QTextStream out(&file);
    out << Producto::act_id() << "\n";
    file.close();
    delete ui;
}

void MainWindow::on_b_anadir_clicked()
{
    v_anadir = new Anadir();
    v_anadir->setAttribute(Qt::WA_DeleteOnClose);
    v_anadir->setModal(true);
    //connect(ventana,&Uno::accepted,this,&MainWindow::actualizar);
    v_anadir->show();
}

void MainWindow::on_b_listar_clicked()
{
    v_listar = new Listar();
    v_listar->setAttribute(Qt::WA_DeleteOnClose);
    v_listar->setModal(true);
    v_listar->init();
    //connect(ventana,&Uno::accepted,this,&MainWindow::actualizar);
    v_listar->show();

}
