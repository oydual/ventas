#ifndef LISTAR_H
#define LISTAR_H

#include <QDialog>
#include <producto.h>
#include <data.h>
#include <QVariant>

namespace Ui {
class Listar;
}

class Listar : public QDialog
{
    Q_OBJECT

public:
    explicit Listar(QWidget *parent = 0);
    void init();
    ~Listar();

private:
    Ui::Listar *ui;
};

#endif // LISTAR_H
