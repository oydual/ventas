#ifndef DATA_H
#define DATA_H

#include<QList>
#include <producto.h>
#include <QString>
#include <fstream>
#include <QLinkedList>
class Data
{
public:
    static Data *get();
    QList<Producto>& getListProductos();
    bool write(QString,void*);
    void initProductos();
    QLinkedList<int> list;
protected:
    Data();
private:
    static Data* datos;
    QList<Producto> lista_productos;
    static void borrar();
};

#endif // DATA_H
