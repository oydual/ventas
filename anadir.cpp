#include "anadir.h"
#include "ui_anadir.h"
#include <data.h>
Anadir::Anadir(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Anadir)
{
    ui->setupUi(this);
    this->setWindowTitle("Anadir Producto");
    QRegExp expCant("([0-9]{1,5})");
    QValidator *valCant = new QRegExpValidator(expCant,this);
    ui->le_cantidad->setValidator(valCant);
    QRegExp expPrecio("([0-9]{1,5}\\.[0-9]{1,3})");
    QValidator *valPrecio = new QRegExpValidator(expPrecio,this);
    ui->le_precio->setValidator(valPrecio);
    QRegExp expNombre("([a-z A-Z]{1,40})");
    QValidator *valNombre = new QRegExpValidator(expNombre,this);
    ui->le_nombre->setValidator(valNombre);
}

Anadir::~Anadir()
{
    delete ui;
}

void Anadir::on_baceptar_clicked()
{
    Data *datos;
    datos = Data::get();
    Producto *aux = new Producto(this->ui->le_cantidad->text().toInt(), Producto::act_id() , this->ui->le_precio->text().toFloat(), (Tipo)(this->ui->cb_tipo->currentIndex()),this->ui->le_nombre->text());
    Producto::add_id();
    datos->getListProductos().push_back(*aux);
    datos->write("Producto",aux);
    this->accept();
}
