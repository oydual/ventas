#include "mainwindow.h"
#include <QString>
#include <QTextStream>
#include <QApplication>
#include <QFile>
#include <producto.h>

int main(int argc, char *argv[])
{
    QFile file("config.dat");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    QString line = in.readLine();
    Producto::init(line.toInt());
    file.close();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
