#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QTextStream>
#include <anadir.h>
#include <listar.h>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_b_anadir_clicked();

    void on_b_listar_clicked();

private:
    Ui::MainWindow *ui;
    Anadir *v_anadir;
    Listar *v_listar;
};

#endif // MAINWINDOW_H
