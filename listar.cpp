#include "listar.h"
#include "ui_listar.h"

Listar::Listar(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Listar)
{
    ui->setupUi(this);
}

Listar::~Listar()
{
    delete ui;
}

void Listar::init()
{
    Data *datos = Data::get();
    QList<Producto>::iterator it;
    ui->t_productos->clearContents();
    ui->t_productos->setRowCount(0);
    int i = 0;
    QTableWidgetItem *item;
    for (it = datos->getListProductos().begin(); it != datos->getListProductos().end(); ++it)
    {
        item = new QTableWidgetItem();
        ui->t_productos->insertRow(ui->t_productos->rowCount());
        item->setData(Qt::DisplayRole,QVariant((*it).getId()));
        ui->t_productos->setItem(i,0,item);
        item = new QTableWidgetItem();
        item->setData(Qt::DisplayRole,QVariant((*it).getTipoS()));
        ui->t_productos->setItem(i,1,item);
        ui->t_productos->setItem(i,2,new QTableWidgetItem((*it).getNombre()));
        item = new QTableWidgetItem();
        item->setData(Qt::DisplayRole,QVariant((*it).getCant()));
        ui->t_productos->setItem(i,3,item);
        item = new QTableWidgetItem();
        item->setData(Qt::DisplayRole,QVariant((*it).getPrecio()));
        ui->t_productos->setItem(i,4,item);
        i++;
    }
}
